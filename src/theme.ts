// The Open Network Theme
const theme = {
  config: {
    useColorSchemeMediaQuery: "system",
  },
  colors: {
    text: "#232129",
    primary: "#0088CC",
    background: "#F7F9FB",
    secondary: "#6AAEFF",
    gradient: "#B2D4FC",
    readonly: "#D9D7E0",
    modes: {
      dark: {
        text: "#FBFBFB",
        background: "#232328",
        readonly: "#36313D",
      },
    },
  },
  fonts: {
    body: "system-ui, sans-serif",
    heading: "system-ui, sans-serif",
    monospace: "Menlo, monospace",
  },
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.125,
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 72],
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
};

export default theme;

import * as React from "react";
import { HeadFC, PageProps, navigate } from "gatsby";
import { Box, Button, Container, Heading, Paragraph, Text } from "theme-ui";
import { useState, useEffect } from "react";
import axios from "axios";

const IndexPage: React.FC<PageProps> = () => {
  // Client-side Runtime Data Fetching
  const [sitesCount, setSitesCount] = useState(0);
  useEffect(() => {
    const getSitesCount = async () => {
      try {
        // get data from the Provider
        const { data } = await axios.get(
          `${process.env.TWAWS_URL}/provider/info`
        );
        console.log(process.env.TONCENTER_HTTP_API_V2_URL);
        console.log(process.env.NODE_ENV);
        if (!data.ok || data.code !== 0) {
          return;
        }
        setSitesCount(data.result.contracts_count);
      } catch (error: any) {
        return error.message;
      }
    };
    getSitesCount();
  }, []);

  return (
    <Container p={5} sx={{ display: "grid", gridGap: 3 }}>
      <Box>
        <Heading
          as={"h1"}
          sx={{ color: "text", lineHeight: "heading", fontWeights: "heading" }}
        >
          <Text sx={{ color: "text" }}>Status Of</Text>
          <br />
          <Text sx={{ color: "primary" }}>TON Web Archive.</Text>
        </Heading>
        <Heading
          as={"h2"}
          sx={{ color: "text", lineHeight: "heading", fontWeights: "heading" }}
        >
          <Text sx={{ color: "primary" }}>{sitesCount} TON Sites</Text>
        </Heading>
        <Heading
          as={"h3"}
          sx={{ color: "text", lineHeight: "heading", fontWeights: "heading" }}
        >
          <Text sx={{ color: "text" }}>are live.</Text>
        </Heading>
      </Box>
      <Box>
        <Heading
          as={"h1"}
          sx={{ color: "text", lineHeight: "heading", fontWeights: "heading" }}
        >
          <Text sx={{ color: "text" }}>Migration Tool</Text>
          <br />
          <Text sx={{ color: "primary" }}>
            — to The Open Network (.TON) Site.
          </Text>
        </Heading>
        <p></p>
        <Paragraph
          variant={"block"}
          sx={{ color: "text", lineHeight: "body", fontWeights: "body" }}
        >
          <Paragraph>On November 30, 2022, OpenAI launched ChatGPT.</Paragraph>
          <Paragraph>
            GPTs are being trained from the World Wide Web, or the internet.
          </Paragraph>
          <Paragraph>
            In order to keep personal data privately from the big tech and
          </Paragraph>
          <Paragraph>
            circumvent surveillance from the public internet (the "Clearnet"),
          </Paragraph>
          <Paragraph>
            the author has launched a social movement called The Open Network
          </Paragraph>
          <Paragraph>
            Web Archive Project (colloquially "TON Web Archive"), to help
          </Paragraph>
          <Paragraph>
            migrate static websites to the .TON domains, which are guarded by
          </Paragraph>
          <Paragraph>
            TON Proxy and the contents are isolated from arbitrary crawling.
          </Paragraph>
          <Paragraph>
            Anyone can use the service to migrate static websites and contents
          </Paragraph>
          <Paragraph>
            to a .TON domain, which will be owned by the content uploder and
          </Paragraph>
          <Paragraph>
            the contents will be stored decentralized on TON Storage and can be
          </Paragraph>
          <Paragraph>verified on TON Blockchain.</Paragraph>
          <Paragraph>
            One shall use magic to defeat magic, when it comes to GPTs.
          </Paragraph>
        </Paragraph>
        <p></p>
        <Button
          sx={{
            color: "background",
            fontWeight: "bold",
            "&:hover": { color: "secondary", cursor: "pointer" },
          }}
          onClick={() => {
            navigate("/journey/intro");
          }}
        >
          Start.
        </Button>
      </Box>
    </Container>
  );
};

export default IndexPage;

export const Head: HeadFC = () => <title>Home of TON Web Archive</title>;

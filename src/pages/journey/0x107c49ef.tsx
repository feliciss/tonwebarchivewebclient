import * as React from "react";
import { Router, Link, Location } from "@reach/router";
import { PageProps, HeadFC, navigate } from "gatsby";
import Layout from "../components/layout";
import Bag from "../components/0x107c49ef/bag";
import Storage from "../components/0x107c49ef/storage";
import Contract from "../components/0x107c49ef/contract";

import { TonConnectUIProvider } from "@tonconnect/ui-react";
import { Heading, Paragraph } from "theme-ui";
import Navigation from "../components/navigation";

const Journey0x107c49efPage: React.FC<PageProps> = () => (
  <TonConnectUIProvider manifestUrl={`${process.env.MANIFEST_URL_FILE}`}>
    <div>
      <Layout siteTitle={"0x107c49ef"}>
        <Heading sx={{ color: "text" }} as={"h1"}>
          👋 Journey <b>{"0x107c49ef"}</b>
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The journey will guide on how to store a static website on TON Storage
          by creating a storage contract aggrement with a storage provider from
          TON Web Archive.
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h2"}>
          🤔️ Do you know?
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          Creating a storage contract aggrement is to call an operation{" "}
          <code>op::offer_storage_contract</code>, its operation code is{" "}
          <code>0x107c49ef</code>.
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h2"}>
          🙌 Let's do it!
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The hands-on section will guide on deploying a storage contract that
          stores static website files, including <b>index.html</b>, on TON
          Storage in 3 steps.
        </Paragraph>
        <br />
        <Bag></Bag>
        <br />
        <Storage></Storage>
        <br />
        <Contract></Contract>
        <br />
        <Heading sx={{ color: "text" }} as={"h2"}>
          👩‍🎨 Fin.
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          <Paragraph>
            Upon completion of the Journey, there are a static website being
            stored on TON Storage and a .TON domain pointing to the static
            website.
          </Paragraph>
          <Paragraph>
            Coins will be automatically deducted from the storage contract
            address to the Provider's contract address following the aggrement.
          </Paragraph>
          <Paragraph>
            Keep in mind, when the storage contract address has no suffcient
            coins to maintain the storage cost being defined by the Provider,
            the contract
          </Paragraph>
          <Paragraph>
            will be closed and all Toncoin on the contract will be transferred
            to the Provider's address.
          </Paragraph>
          <Paragraph>
            Thus, it's strongly recommended to deposit enough Toncoin to the
            storage contract address to keep the static websites stored on TON
            Storage and the .TON site alive.
          </Paragraph>
        </Paragraph>
        <br />
        <Navigation
          isFirst={false}
          isLast={true}
          props={{ to: "/journey/intro" }}
        ></Navigation>
      </Layout>
    </div>
  </TonConnectUIProvider>
);

export default Journey0x107c49efPage;

export const Head: HeadFC = () => <title>Journey of TON Web Archive</title>;

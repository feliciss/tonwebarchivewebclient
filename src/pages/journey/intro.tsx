import * as React from "react";
import { Router, Link, Location } from "@reach/router";
import { PageProps, HeadFC, navigate } from "gatsby";
import Layout from "../components/layout";
import Bag from "../components/0x107c49ef/bag";
import Storage from "../components/0x107c49ef/storage";
import Contract from "../components/0x107c49ef/contract";

import { TonConnectUIProvider } from "@tonconnect/ui-react";
import { Heading, Paragraph } from "theme-ui";
import Navigation from "../components/navigation";

const JourneyIntroPage: React.FC<PageProps> = () => (
  <TonConnectUIProvider manifestUrl={`${process.env.MANIFEST_URL_FILE}`}>
    <div>
      <Layout siteTitle={"Journey"}>
        <Heading sx={{ color: "text" }} as={"h1"}>
          📖 Introduction of TON Web Archive <b>Journey</b>
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Open Network Web Archive Project (the "Project") is a
          service-educational content provider, and TON Web Archive Journey (the
          "Journey") focuses on educational contents which will guide from
          uploading a static website to owning a self-custody TON DNS domain
          which directs to the static website stored on TON Storage.
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h2"}>
          🧐 Structures of the Journey
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Journey consists of TON Blockchain operation codes in hexadecimal
          notations start with <code>0x</code>.
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h3"}>
          <code>0x107c49ef</code>
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          In this Journey, one will upload a static website through the web
          interface, get a Bag ID on TON Storage, generate a storage request
          from the Bag ID, and deploy a storage contract on TON Blockchain from
          the storage request.
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h2"}>
          🌐 Services for the Journey
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The services provided for the Journey are based on The Open Network.
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h3"}>
          Web Services
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Project connects with web services to request and receive data
          from TON Blockchain.
        </Paragraph>
        <Heading sx={{ color: "text" }} as={"h4"}>
          TON Connect
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Project uses TON Connect to connect wallets on TON Blockchain.
          Click the Connect Wallet located in the top right corner to connect to
          wallets.
        </Paragraph>
        <Heading sx={{ color: "text" }} as={"h4"}>
          TON HTTP API
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Project uses TON Center as the TON HTTP API provider to fetch and
          request data from TON Blockchain.
        </Paragraph>
        <Heading sx={{ color: "text" }} as={"h4"}>
          TON Web Archive Web Server
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Project hosts a web server to handle requests to TON Storage. The
          server code is open sourced on{" "}
          <a
            href={`${process.env.ACAB_ONION_URL}/feliciss/tonwebarchivewebsever`}
            target="_blank"
            rel="noopener noreferrer"
          >
            0xacab
          </a>{" "}
          (an .onion address).
        </Paragraph>
        <br />
        <Heading sx={{ color: "text" }} as={"h3"}>
          Decentralized Services
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Project also connects with decentralized services to provide
          access to TON Blockchain.
        </Paragraph>
        <Heading sx={{ color: "text" }} as={"h4"}>
          The Open Network Wayback Machine
        </Heading>
        <Paragraph sx={{ color: "text" }}>
          The Open Network Wayback Machine, or TON Wayback Machine,{" "}
          <a
            href={`${process.env.WAYBACKMACHINE_URL}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            waybackmachine.ton
          </a>
          , is a non-profit sibling project of TON Web Archive, which hosts
          static website contents for free on TON Blockchain.
        </Paragraph>
        <br />
        <Navigation
          isFirst={true}
          isLast={false}
          props={{ to: "/journey/0x107c49ef" }}
        ></Navigation>
      </Layout>
    </div>
  </TonConnectUIProvider>
);

export default JourneyIntroPage;

export const Head: HeadFC = () => <title>Journey of TON Web Archive</title>;

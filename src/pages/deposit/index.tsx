import { HeadFC, PageProps } from "gatsby";
import * as React from "react";

const DepositPage: React.FC<PageProps> = () => {
  return <></>;
};

export default DepositPage;

export const Head: HeadFC = () => <title>Deposit of TON Web Archive</title>;

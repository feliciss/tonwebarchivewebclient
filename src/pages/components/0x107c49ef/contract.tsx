import * as React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import { useTonAddress, useTonConnectUI } from "@tonconnect/ui-react";
import { object, string } from "yup";
import {
  Alert,
  Badge,
  Close,
  Heading,
  Message,
  NavLink,
  Paragraph,
} from "theme-ui";
import { Input } from "theme-ui";
import { Button } from "theme-ui";
import { Container } from "theme-ui";
import axios from "axios";
import { Buffer } from "buffer";
import { useState, useEffect, useRef } from "react";

const Contract = () => {
  const codeRef = useRef<HTMLPreElement>(null);
  const [copied, setCopied] = useState(false);

  const copyCode = () => {
    if (codeRef.current) {
      navigator.clipboard
        .writeText(codeRef.current.textContent || "")
        .then(() => {
          // Code copied successfully
          setCopied(true);
          setTimeout(() => {
            setCopied(false);
          }, 2000);
        })
        .catch((error) => {
          // Failed to copy code to clipboard
        });
    }
  };

  const [tonConnectUI] = useTonConnectUI();
  const rawAddress = useTonAddress(false);

  /* global BigInt */
  const [deployAmount, setDeployAmount] = useState(0);
  const [minContractAmount, setMinDeployAmount] = useState(50000000);

  useEffect(() => {
    const estimateFee = async () => {
      // mock up requests
      const init_code = process.env.ESTIMATE_FEE_INIT_CODE;
      const init_data = process.env.ESTIMATE_FEE_INIT_DATA;
      const body = process.env.ESTIMATE_FEE_INIT_BODY;
      if (rawAddress) {
        const { data } = await axios.post(
          `${process.env.TONCENTER_HTTP_API_V2_URL}/estimateFee`,
          {
            address: rawAddress,
            body,
            init_code,
            init_data,
            ignore_chksig: true,
          }
        );
        if (!data.ok) {
          return { request: "Error estimating fee." };
        } else {
          // set deployment cost
          const cost =
            data.result.source_fees.in_fwd_fee +
            data.result.source_fees.storage_fee +
            data.result.source_fees.gas_fee +
            data.result.source_fees.fwd_fee;
          setDeployAmount(cost);
        }
      } else {
        return;
      }
    };
    estimateFee();
  }, []);

  return (
    <div>
      <Heading sx={{ color: "text" }} as={"h3"}>
        Step 3: Create a storage contract from a storage request.
      </Heading>
      <Container p={2}>
        <Message sx={{ bg: "background" }}>
          <Heading sx={{ color: "text" }} as={"h4"}>
            ✍️ Verify before storage contract creation
          </Heading>
          <Paragraph sx={{ color: "text" }}>
            Storage contract aggrements with TON Web Archive Storage Provider
            (the "Provider") are stored on TON Blockchain. Verify the Provider's
            information on{" "}
            <a
              href={`${process.env.TONCX_URL}/address/${process.env.STORAGE_PROVIDER_ADDRESS}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              ton.cx
            </a>{" "}
            to check if it's an authentic storage provider.
          </Paragraph>
        </Message>
        <br />
        <Message sx={{ bg: "background" }} hidden={!rawAddress}>
          <Heading sx={{ color: "text" }} as={"h4"}>
            🧙 Contract cost
          </Heading>
          <Paragraph sx={{ color: "text" }}>
            The storage contract cost is{" "}
            <b>{(minContractAmount + deployAmount) / 1000000000} Toncoin</b> in
            total, consisting of <b>{minContractAmount / 1000000000} Toncoin</b>{" "}
            of contract creation and estimated{" "}
            <b>{deployAmount / 1000000000} Toncoin</b> of contract deployment.
          </Paragraph>
        </Message>
      </Container>
      <Formik
        // init_data can't be predetermined, so use a reference init_data
        initialValues={{ request: "", contractAddress: "", bagId: "" }}
        validationSchema={object().shape({
          request: string()
            .required("A storage request is required.")
            .matches(
              /^(te6ccgEBAgEAjwABaRB8Se8AAAAAAAAAA).*(AA==)$/,
              "Invalid Base64 storage request."
            ),
        })}
        onSubmit={(values, { setSubmitting, setErrors }) => {
          if (deployAmount === 0) {
            setErrors({
              request:
                "Error calculating deployment cost. Please try again reloading the page.",
            });
            return;
          }
          setTimeout(async () => {
            if (rawAddress) {
              try {
                const amountToSend = minContractAmount + deployAmount;
                const transaction = {
                  validUntil: Date.now() + 1000000,
                  messages: [
                    {
                      address: process.env.STORAGE_PROVIDER_ADDRESS
                        ? process.env.STORAGE_PROVIDER_ADDRESS
                        : "",
                      amount: amountToSend.toString(),
                      payload: values.request,
                    },
                  ],
                };
                // different wallet gets different result, ignore processing
                const sendTransactionRespose =
                  await tonConnectUI.sendTransaction(transaction);
                console.log(sendTransactionRespose.boc);
                if (sendTransactionRespose.boc === "ok") {
                  setErrors({
                    request: `Contract created. Please view MyTonWallet to see the newly created contract address and deposit Toncoin to the contract address. The .TON site will be on live on ${process.env.WAYBACKMACHINE_URL}/ followed by the Bag ID.`,
                  });
                } else if (sendTransactionRespose.boc.match(/te6cckEC.*=/)) {
                  setErrors({
                    request: `Contract created. Please view Tonkeeper to see the newly created contract address and deposit Toncoin to the contract address. The .TON site will be on live on ${process.env.WAYBACKMACHINE_URL}/ followed by the Bag ID.`,
                  });
                } else if (sendTransactionRespose.boc) {
                  setErrors({
                    request: `Contract created. Signed Bag of Cell is ${sendTransactionRespose.boc}. View the wallet to see the newly created contract address and deposit Toncoin to the contract address. The .TON site will be on live on ${process.env.WAYBACKMACHINE_URL}/ followed by the Bag ID.`,
                  });
                } else {
                  setErrors({
                    request: `Contract created from unsuported wallet. Please view the wallet to see the newly created contract address and deposit Toncoin to the contract address. The .TON site will be on live on ${process.env.WAYBACKMACHINE_URL}/ followed by the Bag ID.`,
                  });
                  // const { data } = await axios.get(`${process.env.TONCENTER_HTTP_API_V2_URL}/getTransactions?address=${rawAddress}&limit=${1}`)
                  // if (!data.ok) {
                  //   setErrors({request: "Transaction sent, but error on getting latest transaction."})
                  // } else {
                  //   const tx = data.result[0];
                  //   console.log(tx.data)
                  //   if (tx.data === sendTransactionRespose.boc) {
                  //     setFieldValue('contractAddress', tx.in_msg.source)
                  //     const txMessage = tx.in_msg.message;
                  //     const validString = "AAAAAAAAAA";
                  //     if (!txMessage.includes(validString)) {
                  //       setErrors({request: "Invalid transaction message for a Bag ID."})
                  //     } else {
                  //       const base64BagId = txMessage.split(validString).pop();
                  //       const bagId = Buffer.from(base64BagId, 'base64').toString('hex').toUpperCase();
                  //       setFieldValue('bagId', bagId);
                  //       setStatus(true)
                  //     }
                  //   } else {
                  //     setErrors({request: "Error getting the transaction data."})
                  //   }
                  // }
                }
              } catch (error: any) {
                setErrors({ request: "Error handling sending transactions." });
              }
            } else {
              setErrors({ request: "Connect wallet to send transactions." });
            }
            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          status,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <>
            <Form onSubmit={handleSubmit}>
              <Container sx={{ color: "text" }} p={2}>
                Storage Request:
                <Input
                  type="text"
                  name="request"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.request}
                  size={70}
                  placeholder={"Storage Request"}
                  required
                />
                <ErrorMessage name="request" />
              </Container>
              <Container p={2}>
                <Button
                  sx={{
                    color: "background",
                    fontWeight: "bold",
                    "&:hover": { color: "secondary", cursor: "pointer" },
                  }}
                  type="submit"
                  disabled={isSubmitting}
                >
                  Create.
                </Button>
              </Container>
            </Form>
            {status && !isSubmitting ? (
              <Container p={2}>
                <Alert variant="primary">
                  <span style={{ marginRight: "auto" }}>Contract created</span>
                  <Badge ml={2}>
                    <a
                      style={{ color: "#FFF", textDecoration: "none" }}
                      href={`${process.env.TONSCAN_URL}/address/${values.contractAddress}`}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      View
                    </a>
                  </Badge>
                  <Badge ml={1}>
                    <a
                      style={{ color: "#FFF", textDecoration: "none" }}
                      href={`ton://transfer/${values.contractAddress}`}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Deposit
                    </a>
                  </Badge>
                  <span onClick={copyCode} style={{ cursor: "pointer" }}>
                    {copied ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-check2"
                        viewBox="0 0 16 16"
                      >
                        <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-clipboard"
                        viewBox="0 0 16 16"
                      >
                        <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                        <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                      </svg>
                    )}
                  </span>
                </Alert>
                <pre ref={codeRef}>
                  <code>{values.contractAddress}</code>
                </pre>
                <Message sx={{ bg: "background" }}>
                  <Heading sx={{ color: "text" }} as={"h4"}>
                    🧚‍♂️ TON Site lives
                  </Heading>
                  <Paragraph sx={{ color: "text" }}>
                    The .TON site will go live in seconds on the URL{" "}
                    <a
                      href={`${`${process.env.WAYBACKMACHINE_URL}`}/${
                        values.bagId
                      }`}
                      target="_blank"
                      rel="noopener noreferrer"
                    >{`${`${process.env.WAYBACKMACHINE_URL}`}/${
                      values.bagId
                    }`}</a>{" "}
                    hosted by The Open Network Wayback Machine.
                  </Paragraph>
                </Message>
              </Container>
            ) : null}
          </>
        )}
      </Formik>
    </div>
  );
};
export default Contract;

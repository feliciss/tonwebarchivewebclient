import * as React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import { object, string } from "yup";
import axios from "axios";
import {
  Alert,
  Heading,
  Input,
  Message,
  Paragraph,
  Spinner,
  Container,
  Button,
} from "theme-ui";
import { useRef, useState } from "react";

const Storage = () => {
  const codeRef = useRef<HTMLPreElement>(null);
  const [copied, setCopied] = useState(false);

  const copyCode = () => {
    if (codeRef.current) {
      navigator.clipboard
        .writeText(codeRef.current.textContent || "")
        .then(() => {
          // Code copied successfully
          setCopied(true);
          setTimeout(() => {
            setCopied(false);
          }, 2000);
        })
        .catch((error) => {
          // Failed to copy code to clipboard
        });
    }
  };

  return (
    <div>
      <Heading sx={{ color: "text" }} as={"h3"}>
        Step 2: Enter a Bag ID below to generate a storage request.
      </Heading>
      <Container p={2}>
        <Message sx={{ bg: "background" }}>
          <Heading sx={{ color: "text" }} as={"h4"}>
            🫶 Free and Open Source Software
          </Heading>
          <Paragraph sx={{ color: "text" }}>
            The service is free and open sourced on{" "}
            <a
              href={`${process.env.ACAB_ONION_URL}/feliciss/tonwebarchivewebsever/-/blob/no-masters/routes/storage/request/generate/index.js`}
              target="_blank"
              rel="noopener noreferrer"
            >
              0xacab
            </a>{" "}
            (an .onion address). To support, consider{" "}
            <a
              href={`ton://transfer/${process.env.DONATION_ADDRESS}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              donate
            </a>{" "}
            (a ton:// deeplink).
          </Paragraph>
        </Message>
      </Container>
      <Formik
        initialValues={{
          bagId: "",
          payload: "",
          storageProviderAddress: process.env.STORAGE_PROVIDER_ADDRESS
            ? process.env.STORAGE_PROVIDER_ADDRESS
            : "",
          maxSpan: 0,
          rate: 0,
        }}
        validationSchema={object().shape({
          bagId: string()
            .required("A Bag ID is required.")
            .length(64, "Invalid Bag ID length."),
          storageProviderAddress: string().required(
            "A storage provider address is required."
          ),
        })}
        onSubmit={(
          values,
          { setSubmitting, setFieldValue, setStatus, setErrors }
        ) => {
          setTimeout(async () => {
            try {
              const { data } = await axios.post(
                `${process.env.TWAWS_URL}/storage/request/generate`,
                {
                  bagId: values.bagId,
                  storageProviderAddress: values.storageProviderAddress,
                }
              );
              if (!data.ok || data.code !== 0) {
                setErrors({ bagId: data.error });
              } else {
                setFieldValue("payload", data.result.payload);
                setFieldValue("maxSpan", data.result.maxSpan);
                setFieldValue("rate", data.result.rate);
                setStatus(true);
              }
            } catch (error: any) {
              setErrors({ bagId: error.message });
            }
            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          status,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => {
          return (
            <>
              <Form onSubmit={handleSubmit}>
                <Container sx={{ color: "text" }} p={2}>
                  Bag ID:
                  <Input
                    type="text"
                    name="bagId"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.bagId}
                    placeholder={"Bag ID"}
                    required
                  />
                  <ErrorMessage name="bagId" />
                </Container>
                <Container sx={{ color: "text" }} p={2}>
                  Storage Provider Address:
                  <Input
                    sx={{ bg: "readonly", cursor: "not-allowed" }}
                    type="text"
                    name="storageProviderAddress"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={process.env.STORAGE_PROVIDER_ADDRESS}
                    placeholder={"Storage provider address"}
                    required
                    readOnly
                  />
                  <ErrorMessage name="storageProviderAddress" />
                </Container>
                <Container p={2}>
                  <Button
                    sx={{
                      color: "background",
                      fontWeight: "bold",
                      "&:hover": { color: "secondary", cursor: "pointer" },
                    }}
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Generate.
                  </Button>
                </Container>
              </Form>
              {isSubmitting ? <Spinner /> : null}
              {status && !isSubmitting ? (
                <Container p={2}>
                  <Alert variant="primary">
                    <span style={{ marginRight: "auto" }}>Storage request</span>
                    <span onClick={copyCode} style={{ cursor: "pointer" }}>
                      {copied ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-check2"
                          viewBox="0 0 16 16"
                        >
                          <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
                        </svg>
                      ) : (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-clipboard"
                          viewBox="0 0 16 16"
                        >
                          <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                          <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                        </svg>
                      )}
                    </span>
                  </Alert>
                  <pre ref={codeRef}>
                    <code>{values.payload}</code>
                  </pre>
                  <Message sx={{ bg: "background" }}>
                    <Heading sx={{ color: "text" }} as={"h4"}>
                      🧙‍♀️ Storage cost
                    </Heading>
                    <Paragraph sx={{ color: "text" }}>
                      Custodial storage cost is{" "}
                      <b>
                        {(values.maxSpan / 60 / 60 / 24) *
                          (values.rate / 1000000000)}{" "}
                        Toncoin
                      </b>{" "}
                      every <b>{values.maxSpan / 60 / 60 / 24} day(s)</b> based
                      on the storage request fabric generated from the Bag ID{" "}
                      <code>{values.bagId}</code>. If accept this rate, proceed
                      to the next step.
                    </Paragraph>
                  </Message>
                </Container>
              ) : null}
            </>
          );
        }}
      </Formik>
    </div>
  );
};
export default Storage;

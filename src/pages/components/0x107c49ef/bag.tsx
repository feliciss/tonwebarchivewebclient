import { object, array } from "yup";
import { Formik, Form, ErrorMessage } from "formik";
import axios from "axios";
import * as React from "react";
import {
  Container,
  Heading,
  Input,
  Message,
  Paragraph,
  Button,
  Label,
  Alert,
  Spinner,
  Close,
  Divider,
} from "theme-ui";
import { Buffer } from "buffer";
import { useRef, useState } from "react";

const Bag = () => {
  const codeRef = useRef<HTMLPreElement>(null);
  const [copied, setCopied] = useState(false);

  const copyCode = () => {
    if (codeRef.current) {
      navigator.clipboard
        .writeText(codeRef.current.textContent || "")
        .then(() => {
          // Code copied successfully
          setCopied(true);
          setTimeout(() => {
            setCopied(false);
          }, 2000);
        })
        .catch((error) => {
          // Failed to copy code to clipboard
        });
    }
  };

  const checkIfFilesExceedsLimit = (files?: Array<File>): boolean => {
    let valid = true;
    let totalSize = 0;
    if (files) {
      files.map((file: File) => {
        totalSize += file.size;
      });
    }
    if (totalSize > 10485760) {
      valid = false;
    }
    return valid;
  };

  const checkIfRootFolderContainsIndexFile = (files?: Array<File>): boolean => {
    let valid = false;
    if (files) {
      files.map((file: File) => {
        const filepaths = file.webkitRelativePath.split("/");
        if (
          filepaths.length === 2 &&
          ["index.html", "index.htm"].includes(file.name)
        ) {
          valid = true;
        }
      });
    }
    return valid;
  };

  const checkIfFolderContainsInvalidCharacters = (
    files?: Array<File>
  ): boolean => {
    let valid = true;
    if (files) {
      files.map((file: File) => {
        const filepaths = file.webkitRelativePath.split("/");
        if (filepaths.pop()) {
          // remove the file from the paths
          for (const foldername of filepaths) {
            if (!foldername.match(/^[\w]+$/)) {
              valid = false;
            }
          }
        }
      });
    }
    return valid;
  };

  return (
    <div>
      <Heading sx={{ color: "text" }} as={"h3"}>
        Step 1: Upload a website folder to generate a Bag ID and preview TON
        Sites.
      </Heading>
      <Container p={2}>
        <Message sx={{ bg: "background" }}>
          <Heading sx={{ color: "text" }} as={"h4"}>
            📝 Fact sheet
          </Heading>
          <Paragraph sx={{ color: "text" }}>
            🧐 Use the{" "}
            <a
              href={`https://github.com/xssnick/Tonutils-Proxy`}
              target="_blank"
              rel="noopener noreferrer"
            >
              TON Proxy
            </a>{" "}
            to preview .ton websites.
          </Paragraph>
          <Paragraph sx={{ color: "text" }}>
            🙅‍♀️ Websites will not be saved permanently and may be deleted at
            anytime.
          </Paragraph>
          <Paragraph sx={{ color: "text" }}>
            🫶 The service is free and open sourced on{" "}
            <a
              href={`${process.env.ACAB_ONION_URL}/feliciss/tonwebarchivewebsever/-/blob/no-masters/routes/bag/id/generate/index.js`}
              target="_blank"
              rel="noopener noreferrer"
            >
              0xacab
            </a>{" "}
            (an .onion address).
          </Paragraph>
          <Paragraph sx={{ color: "text" }}>
            🤞 To support, consider{" "}
            <a
              href={`ton://transfer/${process.env.DONATION_ADDRESS}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              donate
            </a>{" "}
            (a ton:// deeplink).
          </Paragraph>
        </Message>
      </Container>
      <Formik
        initialValues={{
          files: [],
          bagId: "",
        }}
        validationSchema={object().shape({
          files: array()
            .min(1, "A folder is required.")
            .test("size", "Files exceeds 10 MB", checkIfFilesExceedsLimit)
            .test(
              "filename",
              "Root folder must contain index.html or index.htm files.",
              checkIfRootFolderContainsIndexFile
            )
            .test(
              "foldername",
              "Folder names must be in a valid form. Equivalent to regex \\w.",
              checkIfFolderContainsInvalidCharacters
            ),
        })}
        onSubmit={(
          values,
          { setSubmitting, setFieldValue, setStatus, setErrors }
        ) => {
          setTimeout(async () => {
            const formData = new FormData();
            values.files.forEach((file, index) => {
              formData.append(`${file}`, values.files[index]);
            });
            try {
              const { data } = await axios.post(
                `${process.env.TWAWS_URL}/bag/id/generate`,
                formData,
                {
                  headers: {
                    "Content-Type": "multipart/form-data",
                  },
                }
              );
              if (!data.ok || data.code !== 0) {
                setErrors({ files: data.error });
              } else {
                setFieldValue(
                  "bagId",
                  Buffer.from(data.result.torrent.hash, "base64")
                    .toString("hex")
                    .toUpperCase()
                );
                setStatus(true);
              }
            } catch (error: any) {
              setErrors({ files: error.message });
            }
            setSubmitting(false);
          }, 2000);
        }}
      >
        {({ values, status, isSubmitting, setFieldValue }) => {
          return (
            <>
              <Form>
                <Container sx={{ color: "text" }} p={2}>
                  <Input
                    id="file"
                    name="files"
                    type="file"
                    onChange={(event) => {
                      const files = event.target.files
                        ? event.target.files
                        : [];
                      setFieldValue("files", Array.from(files));
                    }}
                    multiple={true}
                    /* @ts-expect-error */
                    webkitdirectory="true"
                  />
                  <ErrorMessage name="files" />
                </Container>
                <Container p={2}>
                  <Button
                    sx={{
                      color: "background",
                      fontWeight: "bold",
                      "&:hover": { color: "secondary", cursor: "pointer" },
                    }}
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Upload.
                  </Button>
                </Container>
              </Form>
              {isSubmitting ? <Spinner /> : null}
              {status && !isSubmitting ? (
                <Container p={2}>
                  <Alert variant="primary">
                    <span style={{ marginRight: "auto" }}>Bag ID</span>
                    <span
                      onClick={() =>
                        window.open(
                          `${process.env.WAYBACKMACHINE_URL}/${values.bagId}`,
                          "_blank",
                          "noopener noreferrer"
                        )
                      }
                      style={{ cursor: "pointer", marginRight: "10px" }}
                    >
                      <svg
                        width="22"
                        height="22"
                        viewBox="0 -6 25 42"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32Z"
                          fill="#FFFFFF"
                        />
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M11.5452 10.5764H20.5192C20.8366 10.5764 21.1539 10.623 21.4854 10.7776C21.8829 10.9628 22.0936 11.2549 22.2413 11.4708C22.2528 11.4876 22.2635 11.5049 22.2734 11.5226C22.447 11.8317 22.5365 12.1653 22.5365 12.5242C22.5365 12.8653 22.4554 13.2368 22.2734 13.5606C22.2717 13.5637 22.27 13.5668 22.2682 13.5699L16.5986 23.309C16.4736 23.5238 16.2434 23.6556 15.9949 23.6547C15.7464 23.6538 15.5172 23.5204 15.3937 23.3048L9.82824 13.5864C9.82664 13.5838 9.82504 13.5811 9.82344 13.5785C9.69606 13.3686 9.49909 13.044 9.46464 12.6251C9.43298 12.24 9.51955 11.854 9.71309 11.5194C9.90664 11.1847 10.198 10.9172 10.5485 10.7534C10.9242 10.5778 11.305 10.5764 11.5452 10.5764ZM15.3017 11.9677H11.5452C11.2984 11.9677 11.2037 11.9829 11.1374 12.0139C11.0458 12.0566 10.9689 12.1269 10.9175 12.2159C10.8661 12.3049 10.8428 12.408 10.8513 12.5111C10.8561 12.5703 10.8802 12.638 11.0225 12.8725C11.0254 12.8774 11.0284 12.8824 11.0312 12.8874L15.3017 20.3445V11.9677ZM16.693 11.9677V20.3813L21.0625 12.8756C21.1118 12.786 21.1452 12.6565 21.1452 12.5242C21.1452 12.417 21.123 12.3238 21.0733 12.2278C21.0211 12.1528 20.9893 12.1131 20.9628 12.0859C20.94 12.0626 20.9224 12.0502 20.8976 12.0386C20.7943 11.9905 20.6886 11.9677 20.5192 11.9677H16.693Z"
                          fill="#0088CC"
                        />
                      </svg>
                    </span>
                    <span
                      onClick={() =>
                        window.open(
                          `${process.env.WAYBACKMACHINE_CLEARNET_URL}/${values.bagId}`,
                          "_blank",
                          "noopener noreferrer"
                        )
                      }
                      style={{ cursor: "pointer", marginRight: "10px" }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-globe2"
                        viewBox="0 0 16 16"
                        style={{ cursor: "pointer" }}
                      >
                        <path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855-.143.268-.276.56-.395.872.705.157 1.472.257 2.282.287V1.077zM4.249 3.539c.142-.384.304-.744.481-1.078a6.7 6.7 0 0 1 .597-.933A7.01 7.01 0 0 0 3.051 3.05c.362.184.763.349 1.198.49zM3.509 7.5c.036-1.07.188-2.087.436-3.008a9.124 9.124 0 0 1-1.565-.667A6.964 6.964 0 0 0 1.018 7.5h2.49zm1.4-2.741a12.344 12.344 0 0 0-.4 2.741H7.5V5.091c-.91-.03-1.783-.145-2.591-.332zM8.5 5.09V7.5h2.99a12.342 12.342 0 0 0-.399-2.741c-.808.187-1.681.301-2.591.332zM4.51 8.5c.035.987.176 1.914.399 2.741A13.612 13.612 0 0 1 7.5 10.91V8.5H4.51zm3.99 0v2.409c.91.03 1.783.145 2.591.332.223-.827.364-1.754.4-2.741H8.5zm-3.282 3.696c.12.312.252.604.395.872.552 1.035 1.218 1.65 1.887 1.855V11.91c-.81.03-1.577.13-2.282.287zm.11 2.276a6.696 6.696 0 0 1-.598-.933 8.853 8.853 0 0 1-.481-1.079 8.38 8.38 0 0 0-1.198.49 7.01 7.01 0 0 0 2.276 1.522zm-1.383-2.964A13.36 13.36 0 0 1 3.508 8.5h-2.49a6.963 6.963 0 0 0 1.362 3.675c.47-.258.995-.482 1.565-.667zm6.728 2.964a7.009 7.009 0 0 0 2.275-1.521 8.376 8.376 0 0 0-1.197-.49 8.853 8.853 0 0 1-.481 1.078 6.688 6.688 0 0 1-.597.933zM8.5 11.909v3.014c.67-.204 1.335-.82 1.887-1.855.143-.268.276-.56.395-.872A12.63 12.63 0 0 0 8.5 11.91zm3.555-.401c.57.185 1.095.409 1.565.667A6.963 6.963 0 0 0 14.982 8.5h-2.49a13.36 13.36 0 0 1-.437 3.008zM14.982 7.5a6.963 6.963 0 0 0-1.362-3.675c-.47.258-.995.482-1.565.667.248.92.4 1.938.437 3.008h2.49zM11.27 2.461c.177.334.339.694.482 1.078a8.368 8.368 0 0 0 1.196-.49 7.01 7.01 0 0 0-2.275-1.52c.218.283.418.597.597.932zm-.488 1.343a7.765 7.765 0 0 0-.395-.872C9.835 1.897 9.17 1.282 8.5 1.077V4.09c.81-.03 1.577-.13 2.282-.287z" />
                      </svg>
                    </span>
                    <span onClick={copyCode} style={{ cursor: "pointer" }}>
                      {copied ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-check2"
                          viewBox="0 0 16 16"
                        >
                          <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
                        </svg>
                      ) : (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-clipboard"
                          viewBox="0 0 16 16"
                        >
                          <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                          <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                        </svg>
                      )}
                    </span>
                  </Alert>
                  <pre ref={codeRef}>
                    <code>{values.bagId}</code>
                  </pre>
                </Container>
              ) : null}
            </>
          );
        }}
      </Formik>
    </div>
  );
};

export default Bag;

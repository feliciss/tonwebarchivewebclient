import { GatsbyLinkProps, Link, PageProps } from "gatsby";
import React from "react";
import { Box, Button, Container, LinkProps } from "theme-ui";

const Navigation = ({
  isFirst,
  isLast,
  props,
}: {
  isFirst: boolean;
  isLast: boolean;
  props: any;
}) => (
  <>
    <Container
      sx={{
        display: "grid",
        gridGap: 2,
        gridTemplateColumns: "repeat(1, 1fr)",
      }}
    >
      <Box
        hidden={isFirst}
        sx={{
          gridColumnStart: "1",
          gridColumnEnd: "1",
        }}
      >
        <Link {...props} activeClassName="active">
          Previous
        </Link>
      </Box>
      <Box
        hidden={isLast}
        sx={{
          gridColumnStart: "2",
          gridColumnEnd: "2",
        }}
      >
        <Link {...props} activeClassName="active">
          Next
        </Link>
      </Box>
    </Container>
  </>
);
export default Navigation;

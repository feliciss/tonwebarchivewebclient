/** @jsx jsx */
import { jsx } from "theme-ui";
import * as React from "react";
import { TonConnectButton, TonConnectUIProvider } from "@tonconnect/ui-react";
import { Link } from "gatsby";

const Header = ({ siteTitle }: { siteTitle: React.ReactNode }) => (
  <TonConnectUIProvider manifestUrl={`${process.env.MANIFEST_URL_FILE}`}>
    <header
      style={{
        margin: `0 auto`,
        padding: `var(--space-4) var(--size-gutter)`,
        display: `flex`,
        alignItems: `center`,
        justifyContent: `space-between`,
      }}
    >
      <div className="logo">
        <Link className="logo__link" to="/">
          <svg
            width="164"
            height="48"
            viewBox="0 0 164 26"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32Z"
              fill="#0088CC"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M11.5452 10.5764H20.5192C20.8366 10.5764 21.1539 10.623 21.4854 10.7776C21.8829 10.9628 22.0936 11.2549 22.2413 11.4708C22.2528 11.4876 22.2635 11.5049 22.2734 11.5226C22.447 11.8317 22.5365 12.1653 22.5365 12.5242C22.5365 12.8653 22.4554 13.2368 22.2734 13.5606C22.2717 13.5637 22.27 13.5668 22.2682 13.5699L16.5986 23.309C16.4736 23.5238 16.2434 23.6556 15.9949 23.6547C15.7464 23.6538 15.5172 23.5204 15.3937 23.3048L9.82824 13.5864C9.82664 13.5838 9.82504 13.5811 9.82344 13.5785C9.69606 13.3686 9.49909 13.044 9.46464 12.6251C9.43298 12.24 9.51955 11.854 9.71309 11.5194C9.90664 11.1847 10.198 10.9172 10.5485 10.7534C10.9242 10.5778 11.305 10.5764 11.5452 10.5764ZM15.3017 11.9677H11.5452C11.2984 11.9677 11.2037 11.9829 11.1374 12.0139C11.0458 12.0566 10.9689 12.1269 10.9175 12.2159C10.8661 12.3049 10.8428 12.408 10.8513 12.5111C10.8561 12.5703 10.8802 12.638 11.0225 12.8725C11.0254 12.8774 11.0284 12.8824 11.0312 12.8874L15.3017 20.3445V11.9677ZM16.693 11.9677V20.3813L21.0625 12.8756C21.1118 12.786 21.1452 12.6565 21.1452 12.5242C21.1452 12.417 21.123 12.3238 21.0733 12.2278C21.0211 12.1528 20.9893 12.1131 20.9628 12.0859C20.94 12.0626 20.9224 12.0502 20.8976 12.0386C20.7943 11.9905 20.6886 11.9677 20.5192 11.9677H16.693Z"
              fill="white"
            />
            <path
              d="M41.92 25.22H39.48L44.76 8.9H47.2L41.92 25.22Z"
              fill="#728A96"
              fill-opacity="0.32"
            />
            <g transform="translate(13.864 -113.98)">
              <text
                sx={{ fill: "text" }}
                x="38.441856"
                y="137.63289"
                font-family="Inter"
                font-size="18px"
                font-weight="bold"
                stroke-width=".26458"
                xml-space="preserve"
              >
                <tspan x="38.441856" y="137.63289" stroke-width=".26458">
                  Web Archive
                </tspan>
              </text>
            </g>
          </svg>
        </Link>
      </div>
      <TonConnectButton />
    </header>
  </TonConnectUIProvider>
);

export default Header;

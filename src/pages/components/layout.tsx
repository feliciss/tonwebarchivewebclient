/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/how-to/querying-data/use-static-query/
 */

import * as React from "react";
// import LineTo from "react-lineto"
import { Box, Container, Text } from "theme-ui";
import { Link } from "gatsby";

import Header from "./header";
import "./layout.css";

const Layout = ({
  children,
  siteTitle,
}: {
  children: React.ReactNode;
  siteTitle: React.ReactNode;
}) => {
  return (
    <>
      {/* <Container
    sx={{
        display: 'grid',
        gridGap: 1,
        gridTemplateColumns: `repeat(auto-fit, minmax(2px, 1fr))`,
        textAlign: "center",
      }}
    >   
        <Box className="0" sx={{
            gridColumnStart: '1',
            gridColumnEnd: '1',
            m: 'auto',
            mt: `var(--space-3)`,
            display: "flex",
            width: 24,
            height: 24,
            bg: 'primary',
            borderRadius: "50%",
        }}>
          <Link to="/journey" style={{color: '#fff', marginLeft: 7.5, textDecoration: "none"}}>0</Link>
            <LineTo delay={0}  fromAnchor="100 50" toAnchor="0 50" from="0" to="1" borderColor="#0088CC" borderWidth={2} borderStyle="solid" />
        </Box>
        
        <Box className="1" sx={{
            gridColumnStart: '2',
            gridColumnEnd: '2',
            m: 'auto',
            mt: `var(--space-3)`,
            display: "flex",
            width: 24,
            height: 24,
            bg: 'primary',
            borderRadius: "50%",
        }}>
            <Link to="/journey/0x107c49ef" style={{color: '#fff', marginLeft: 7.5, textDecoration: "none"}}>1</Link>
            <LineTo delay={0} fromAnchor="100 50" toAnchor="0 50" from="1" to="2" borderColor="#0088CC" borderWidth={2} borderStyle="solid" />
        </Box> */}

      {/* <Box className="2" sx={{
            gridColumnStart: '3',
            gridColumnEnd: '3',
            m: 'auto',
            display: "flex",
            width: 24,
            height: 24,
            bg: 'primary',
            borderRadius: "50%",
        }}>
            <Link style={{color: '#fff', marginLeft: 7.5, textDecoration: "none"}} to="journey/0xbf7bd0c1">2</Link>
            <LineTo delay={0} fromAnchor="100 50" toAnchor="0 50" from="2" to="3" borderColor="#0088CC" borderWidth={2} borderStyle="solid" />
         </Box>

        <Box className="3" sx={{
            gridColumnStart: '4',
            gridColumnEnd: '4',
            m: 'auto',
            display: "flex",
            width: 24,
            height: 24,
            bg: 'primary',
            borderRadius: "50%",
        }}>
            <Link style={{color: '#fff', marginLeft: 7.5, textDecoration: "none"}} to="journey/0x7a361688">3</Link>
            <LineTo delay={0} fromAnchor="100 50" toAnchor="0 50" from="3" to="4" borderColor="#0088CC" borderWidth={2} borderStyle="solid" />
        </Box>

        <Box className="4" sx={{
            gridColumnStart: '5',
            gridColumnEnd: '5',
            m: 'auto',
            display: "flex",
            width: 24,
            height: 24,
            bg: 'primary',
            borderRadius: "50%",
        }}>
            <Link style={{color: '#fff', marginLeft: 7.5, textDecoration: "none"}} to="journey/0xd4caedcd">4</Link> 
        </Box>        */}
      {/* </Container> */}
      <Header siteTitle={siteTitle} />
      <div
        style={{
          margin: `0 auto`,
          maxWidth: `var(--size-content)`,
          padding: `var(--size-gutter)`,
        }}
      >
        <main>{children}</main>
        <footer
          style={{
            marginTop: `var(--space-5)`,
            fontSize: `var(--font-sm)`,
          }}
        >
          <Text sx={{ color: "text" }}>
            © {new Date().getFullYear()} &middot; BUIDL with
            {` `}
            <Link to="/">The Open Network Web Archive Project</Link>
          </Text>
        </footer>
      </div>
    </>
  );
};

export default Layout;

<p align="center">
  <a href="https://feliciss.itcouldbewor.se/tonwebarchivewebclient" target="_blank" rel="noopener noreferrer">
      <img alt="The Open Network" src="https://ton.org/download/ton_symbol.svg" width="60" />
    </a>
</p>
<h1 align="center">
  The Open Network Web Archive Web Client
</h1>

## 🚀 TON Web Archive Web Client Quick start 

1.  **Clone the repository.**

    Use the git command to clone the repository.

    ```shell
    git clone https://0xacab.org/feliciss/tonwebarchivewebclient.git
    ```

2.  **Start developing.**

    Navigate into the directory and start it up.

    ```shell
    cd tonwebarchivewebclient/
    npm run develop
    ```

3.  **Open the code and start customizing!**

    The TON Web Archive site is now running at http://localhost:8000!

4.  **Learn more about Gatsby design**

    - [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    - [Tutorials](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    - [Guides](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    - [API Reference](https://www.gatsbyjs.com/docs/api-reference/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    - [Plugin Library](https://www.gatsbyjs.com/plugins?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    - [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

import type { GatsbyConfig } from "gatsby";
import * as dotenv from "dotenv";
dotenv.config({
  path: `.env.${process.env.NODE_ENV}`,
});

const config: GatsbyConfig = {
  pathPrefix: `/${process.env.REPOSITORY_NAME}`,
  siteMetadata: {
    title: `The Open Network Web Archive Project`,
    siteUrl: `${process.env.WEBARCHIVE_URL}`,
  },
  graphqlTypegen: true,
  plugins: [
    {
      resolve: "gatsby-plugin-theme-ui",
      options: {
        preset: require("./src/theme"),
      },
    },
  ],
};

export default config;
